package com.example;

import com.example.domain.model.Product;
import com.example.domain.model.User;

import java.util.List;

public interface BackToStockService {

    void subscribe(User user, Product product);

    List<User> subscribedUsers(Product product);

}
