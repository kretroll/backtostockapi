package com.example;

import com.example.domain.BackToStockAPI;
import com.example.domain.model.Product;
import com.example.domain.model.ProductCategory;
import com.example.domain.model.User;

import java.util.ArrayList;
import java.util.List;

public class StartClass {
    public static void main(String[] args) {
        User user0 = new User();
        User user1 = new User();
        User user2 = new User();
        User user3 = new User();
        User user4 = new User();
        User user5 = new User();
        User user6 = new User();

        List<User> notifyList = new ArrayList<>();

        BackToStockAPI backToStockAPI = new BackToStockAPI();
        Product product1 = new Product("PainKiller", ProductCategory.MEDICAL);
        Product product2 = new Product("IPhone", ProductCategory.DIGITAL);
        Product product3 = new Product("Shekspir", ProductCategory.BOOKS);

        user0.setName("OldUser");
        user0.setPremium(true);
        user0.setAge(78);

        user1.setName("Bill");
        user1.setPremium(false);
        user1.setAge(71);

        user2.setName("Worrick");
        user2.setPremium(true);
        user2.setAge(56);

        user3.setName("Mike");
        user3.setPremium(true);
        user3.setAge(45);

        user4.setName("Keks");
        user4.setPremium(false);
        user4.setAge(12);

        user5.setName("Richa");
        user5.setPremium(true);
        user5.setAge(38);

        user6.setName("Upsi");
        user6.setPremium(false);
        user6.setAge(73);

        backToStockAPI.subscribe(user1, product1);
        backToStockAPI.subscribe(user2, product2);
        backToStockAPI.subscribe(user3, product3);
        backToStockAPI.subscribe(user4, product2);
        backToStockAPI.subscribe(user0, product3);
        backToStockAPI.subscribe(user5, product2);
        backToStockAPI.subscribe(user6, product3);

        notifyList = backToStockAPI.subscribedUsers(product1);
        System.out.println(notifyList);
    }
}
