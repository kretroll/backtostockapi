package com.example.domain;

import com.example.BackToStockService;
import com.example.domain.model.Product;
import com.example.domain.model.ProductCategory;
import com.example.domain.model.User;

import java.util.ArrayList;
import java.util.List;

public class BackToStockAPI implements BackToStockService {

    List<User> premiumUserList = new ArrayList<>();
    List<User> olderSeventyUserList = new ArrayList<>();
    List<User> userList = new ArrayList<>();
    List<User> premiumOlderSeventyUserList = new ArrayList<>();

    List<User> unsubscribedUsers = new ArrayList<>();
    List<Product> productList = new ArrayList<>();

    @Override
    public void subscribe(User user, Product product) {
        if (productList.contains(product)) {
            user.setName(user.getName());
            user.setPremium(user.isPremium());
            user.setAge(user.getAge());
            unsubscribedUsers.add(user);
            for(User everyUser : unsubscribedUsers) {
                if (everyUser.getAge() > 70 && everyUser.isPremium()) {
                    premiumOlderSeventyUserList.add(everyUser);
                } else if (everyUser.isPremium())
                    premiumUserList.add(everyUser);
                else if (everyUser.getAge() > 70) {
                    olderSeventyUserList.add(everyUser);
                } else {
                    userList.add(everyUser);
                }
            }
            unsubscribedUsers.clear();
        } else if(productList.size() == 0){
            productList.add(product);
            unsubscribedUsers.add(user);
        } else if(unsubscribedUsers.size() == 1){
            user.setName(unsubscribedUsers.get(0).getName());
            user.setPremium(unsubscribedUsers.get(0).isPremium());
            user.setAge(unsubscribedUsers.get(0).getAge());
            unsubscribedUsers.remove(0);
            if (user.getAge() > 70 && user.isPremium()) {
                premiumOlderSeventyUserList.add(user);
            } else if (user.isPremium())
                premiumUserList.add(user);
            else if (user.getAge() > 70) {
                olderSeventyUserList.add(user);
            } else {
                userList.add(user);
            }
        }
    }

    @Override
    public List<User> subscribedUsers(Product product) {
        List<User> list = new ArrayList<>();
        productList.clear();
        if (product.getCategory() == ProductCategory.MEDICAL) {
            list.addAll(premiumOlderSeventyUserList);
            list.addAll(olderSeventyUserList);
            list.addAll(premiumUserList);
            list.addAll(userList);
        }
        if(product.getCategory() == ProductCategory.BOOKS){
            list.addAll(premiumOlderSeventyUserList);
            list.addAll(premiumUserList);
            list.addAll(olderSeventyUserList);
            list.addAll(userList);
        }
        if(product.getCategory() == ProductCategory.DIGITAL) {
            list.addAll(premiumOlderSeventyUserList);
            list.addAll(premiumUserList);
            list.addAll(olderSeventyUserList);
            list.addAll(userList);
        }
            return list;
        }
    }

