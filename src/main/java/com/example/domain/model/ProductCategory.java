package com.example.domain.model;

public enum ProductCategory {

    MEDICAL, BOOKS, DIGITAL

}
