package com.example.domain;

import com.example.domain.model.Product;
import com.example.domain.model.ProductCategory;
import com.example.domain.model.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BackToStockAPITest {
    BackToStockAPI backToStockAPI = new BackToStockAPI();



        User user0 = new User();
        User user1 = new User();
        User user2 = new User();
        User user3 = new User();
        User user4 = new User();
        User user5 = new User();
        User user6 = new User();

        Product product1 = new Product("PainKiller", ProductCategory.MEDICAL);
        Product product2 = new Product("IPhone", ProductCategory.DIGITAL);
        Product product3 = new Product("Shekspir", ProductCategory.BOOKS);

        List<User> subscribedUsersListExpected = new ArrayList<>();
        List<User> subscribedUsersListActual = new ArrayList<>();

    @Test
    void subscribeForOneProduct() {

        user0.setName("OldUser");
        user0.setPremium(true);
        user0.setAge(78);

        user1.setName("Bill");
        user1.setPremium(false);
        user1.setAge(71);

        user2.setName("Worrick");
        user2.setPremium(true);
        user2.setAge(56);

        user3.setName("Mike");
        user3.setPremium(true);
        user3.setAge(45);

        user4.setName("Keks");
        user4.setPremium(false);
        user4.setAge(12);

        user5.setName("Richa");
        user5.setPremium(true);
        user5.setAge(38);

        user6.setName("Upsi");
        user6.setPremium(false);
        user6.setAge(73);

        backToStockAPI.subscribe(user0, product1);
        backToStockAPI.subscribe(user1, product2);
        backToStockAPI.subscribe(user2, product2);
        backToStockAPI.subscribe(user3, product3);
        backToStockAPI.subscribe(user4, product3);
        backToStockAPI.subscribe(user0, product3);
        backToStockAPI.subscribe(user5, product3);
        backToStockAPI.subscribe(user6, product2);

        subscribedUsersListExpected = backToStockAPI.subscribedUsers(product1);
        subscribedUsersListActual.add(user0);

        assertEquals(subscribedUsersListExpected.toString(), subscribedUsersListActual.toString());
    }

    @Test
    void subscribeOldUsersForMedicine() {

        user0.setName("OldUser");
        user0.setPremium(true);
        user0.setAge(78);

        user1.setName("Bill");
        user1.setPremium(false);
        user1.setAge(71);

        user2.setName("Worrick");
        user2.setPremium(true);
        user2.setAge(56);

        user3.setName("Mike");
        user3.setPremium(true);
        user3.setAge(45);

        user4.setName("Keks");
        user4.setPremium(false);
        user4.setAge(12);

        user5.setName("Richa");
        user5.setPremium(true);
        user5.setAge(38);

        user6.setName("Upsi");
        user6.setPremium(false);
        user6.setAge(73);

        backToStockAPI.subscribe(user0, product1);
        backToStockAPI.subscribe(user1, product1);
        backToStockAPI.subscribe(user2, product2);
        backToStockAPI.subscribe(user3, product3);
        backToStockAPI.subscribe(user4, product1);
        backToStockAPI.subscribe(user0, product2);
        backToStockAPI.subscribe(user5, product3);
        backToStockAPI.subscribe(user6, product1);

        subscribedUsersListExpected = backToStockAPI.subscribedUsers(product1);
        subscribedUsersListActual.add(user0);
        subscribedUsersListActual.add(user1);
        subscribedUsersListActual.add(user6);
        subscribedUsersListActual.add(user4);

        assertEquals(subscribedUsersListExpected.toString(), subscribedUsersListActual.toString());
        }

    @Test
    void subscribeOldUsersCheckMediumPriority() {

        user0.setName("OldUser");
        user0.setPremium(true);
        user0.setAge(78);

        user1.setName("Bill");
        user1.setPremium(false);
        user1.setAge(71);

        user2.setName("Worrick");
        user2.setPremium(true);
        user2.setAge(56);

        user3.setName("Mike");
        user3.setPremium(true);
        user3.setAge(45);

        user4.setName("Keks");
        user4.setPremium(false);
        user4.setAge(12);

        user5.setName("Richa");
        user5.setPremium(true);
        user5.setAge(38);

        user6.setName("Upsi");
        user6.setPremium(false);
        user6.setAge(73);

        backToStockAPI.subscribe(user0, product2);
        backToStockAPI.subscribe(user1, product1);
        backToStockAPI.subscribe(user2, product2);
        backToStockAPI.subscribe(user3, product3);
        backToStockAPI.subscribe(user4, product2);
        backToStockAPI.subscribe(user5, product2);
        backToStockAPI.subscribe(user6, product2);

        subscribedUsersListExpected = backToStockAPI.subscribedUsers(product2);
        subscribedUsersListActual.add(user0);
        subscribedUsersListActual.add(user2);
        subscribedUsersListActual.add(user5);
        subscribedUsersListActual.add(user6);
        subscribedUsersListActual.add(user4);

        assertEquals(subscribedUsersListExpected.toString(), subscribedUsersListActual.toString());
    }

    @Test
    void subscribeCategoryBooks() {

        user0.setName("OldUser");
        user0.setPremium(true);
        user0.setAge(78);

        user1.setName("Bill");
        user1.setPremium(false);
        user1.setAge(71);

        user2.setName("Worrick");
        user2.setPremium(false);
        user2.setAge(56);

        user3.setName("Mike");
        user3.setPremium(true);
        user3.setAge(45);

        user4.setName("Keks");
        user4.setPremium(false);
        user4.setAge(12);

        user5.setName("Richa");
        user5.setPremium(false);
        user5.setAge(38);

        user6.setName("Upsi");
        user6.setPremium(false);
        user6.setAge(73);

        backToStockAPI.subscribe(user0, product3);
        backToStockAPI.subscribe(user1, product3);
        backToStockAPI.subscribe(user2, product3);
        backToStockAPI.subscribe(user3, product3);
        backToStockAPI.subscribe(user4, product3);
        backToStockAPI.subscribe(user5, product3);
        backToStockAPI.subscribe(user6, product3);

        subscribedUsersListExpected = backToStockAPI.subscribedUsers(product3);
        subscribedUsersListActual.add(user0);
        subscribedUsersListActual.add(user3);
        subscribedUsersListActual.add(user1);
        subscribedUsersListActual.add(user6);
        subscribedUsersListActual.add(user2);
        subscribedUsersListActual.add(user4);
        subscribedUsersListActual.add(user5);

        assertEquals(subscribedUsersListExpected.toString(), subscribedUsersListActual.toString());
    }

}